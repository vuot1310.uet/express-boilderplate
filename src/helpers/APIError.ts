import httpStatus from 'http-status';
import errorcode from '../constants/errorCode';

/**
 * Class representing an API error.
 * @extends Error
 */
class APIError extends Error {
	status: number;

	message: string;

	errCode: number;

	isOperational: boolean;

	name: 'APIError';

	additionalInfo: string;

	/**
   * Creates an API error.
   * @param {string} message - Error message.
   * @param {number} status - HTTP status code of error.
   * @param {number} errCode - Error code of this error
   */
	constructor(
		message: string,
		status = Number(httpStatus.INTERNAL_SERVER_ERROR),
		errCode = errorcode.EXCEPTION,
		additionalInfo = '',
		stack: string | undefined = undefined
	) {
		super(message);
		this.name = 'APIError';
		this.message = message;
		this.status = status;
		this.errCode = errCode;
		this.isOperational = true;
		this.additionalInfo = additionalInfo;
		if (stack) {
			this.stack = stack;
		}
		// stack trace might include the mention of our custom error class constructor.
		// We don't want to see an error creation in our stack frames, only the real code that caused the issue
		// Error.captureStackTrace(this, APIError);
	}
}

export default APIError;
