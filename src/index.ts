import { createConnection } from 'typeorm';
import app from '../config/express';
import config from '../config/config';
createConnection().then(async () => {

	console.log('Successfully connected to database');
	app.listen(config.port || 3000);
	console.log(`Express server has started on port ${config.port || 3000}. Open http://localhost:${config.port || 3000}/api to see results`);

}).catch(error => console.log(error));
