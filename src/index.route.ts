import { Router } from 'express';

const router = Router();

router.route('/health-check').get((req, res) => {
	res.send('oke');
});

export default router;